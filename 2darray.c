#include <stdio.h>
int main()
{ 
    int i,j,marks[5][3],high[5];
    printf("Enter the marks in 3 courses of 5 students (on 100)\n");
    for(i=0; i<5; i++)
    {
        for(j=0; j<3; j++)
        {
           printf("Enter the marks of subject %d of student %d : ",j+1,i+1);
           scanf("%d",&marks[i][j]);    
        }
    }
    for(i=0; i<5; i++)
    {
       high[i]=0;
       for(j=0; j<3; j++)
       {
           high[i]=high[i]>marks[i][j]?high[i]:marks[i][j];
       }
       printf("Highest marks of student %d = %d\n",i+1,high[i]);    
    }
	return 0;
}